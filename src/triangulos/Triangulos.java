/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triangulos;

import java.util.Scanner;

/**
 *
 * @author Dimas
 */
public class Triangulos {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numfiguras;
        do {
            System.out.print("¿Cuántas figuras quieres?");
            numfiguras = entrada.nextInt();
        } while (numfiguras < 2);
        int nfiguras = numfiguras;
        int numlinea;
        do {
            System.out.print("¿Cuántos asteriscos quieres?");
            numlinea = entrada.nextInt();
        } while (numlinea < 2);
        int numespacios = 0;
        int posasterisco ;
        int nespacios;
        while (numlinea > 0) {
            numfiguras = nfiguras;
            posasterisco = numlinea;
            nespacios = numespacios;
            while (numfiguras > 0) {
            while (posasterisco > 0) {
            System.out.print("*");
            }
            while (nespacios-- > 0) {
                System.out.print(" ");
            }
            if (numfiguras > 1) {
                System.out.print("||");
            }
            numfiguras = numfiguras - 1;
        }
        System.out.println("");
        numlinea = numlinea - 1;
        numespacios = numespacios + 1;
        }
    }  
}